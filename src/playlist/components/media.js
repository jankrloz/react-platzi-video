import React from 'react';

class Media extends React.Component {
    render() {
        const styles = {
            container: {
                color: '#44546b',
                width: 260,
                cursor: 'pointer',
                border: '1px solid red'
            }
        };
        return (
            <div style={styles.container}>
                <div>
                    <img src="" alt="" width={260} height={160}/>
                    <h3>Porque aprender React</h3>
                    <p>Jankrloz Navarrete</p>
                </div>
            </div>
        )
    }
}

export default Media;